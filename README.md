# README #

This repository contains the code source of a XCode 5.1.1 project for an iOS 7, iPhone application, answering the following challenge: https://docs.google.com/document/d/1S2mqX-rX6Jk6CEmq2hRhKCVKeLZ6ilrzAoaQYsnvRSY/edit#.

I haven't used CoreDate for this project, as the amount of data and the usages involved doesn't seem to require cache or offline consultation.

I have choose to keep things as simple as possible, even if the result is not representative of the most I can achieve.

For instance, I have choose to load most of the data after the splash screen, as the time required to do so is short enough.

More work would be necessary to achieve a great use of Auto-Layout features (I usually use more code to set the position of the graphics elements and want to test something new).

Also, It would be nice to add:
- Twitter integration
- Photos Gallery
- Map inside the Studio View.

I have used CocoaPods to manage the external libraries.
http://cocoapods.org/
https://github.com/gekitz/GKPopLoadingView/
https://github.com/jakemarsh/JMImageCache/