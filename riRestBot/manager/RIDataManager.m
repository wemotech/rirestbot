//
//  RIDataManager.m
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 06/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import "RIDataManager.h"

#import "RITeamMember.h"
#import "RITeamMemberProfile.h"
#import "RIStudio.h"

@implementation RIDataManager

@synthesize team;
@synthesize studioData;

const NSString *BASE_URL = @"http://devchallenge.ribot.io/api/";
const NSString *TEAM_MEMBERS_PATH = @"team";
const NSString *STUDIO_PATH = @"studio";
const NSString *TEAM_MEMBER_PROFILE_PATH = @"team/";
///team/<id>/ribotar

+ (NSURL *)urlAvatarTeamMember:(NSString *)ridMember{
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@team/%@/ribotar", BASE_URL, ridMember]];
}

+ (id)dataManager{
    static RIDataManager *sharedDataManager = nil;
    static dispatch_once_t onceToken;
    //I choose to use  Grand Central Dispatch (GCD) but could have also used @synchronized(self)
    dispatch_once(&onceToken, ^{
        sharedDataManager = [[self alloc] init];
    });
    return sharedDataManager;
}

- (void)fetchAllTeamMembers:(void (^)(NSArray *teamMembers))callback{
    NSString *urlString = [NSString stringWithFormat:@"%@%@", BASE_URL, TEAM_MEMBERS_PATH];
    NSURL *url = [NSURL URLWithString:urlString];
    [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:
      ^(NSData *data, NSURLResponse *response, NSError *error) {
          if (error) {
              NSLog(@"error: %@", error.localizedDescription);
              callback(nil);
              return;
          }
          NSError *jsonError = nil;
          id result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
          
          if ([NSStringFromClass([result class]) isEqualToString:@"__NSCFArray"]) {
              
              NSArray *tempRes = (NSArray *) result;
              
              NSMutableArray *jsonRes = [[NSMutableArray alloc] init];
              
              for (NSDictionary *d in tempRes) {
                  RITeamMember *teamMember = [[RITeamMember alloc] init];
                  [teamMember loadFromDictionary:d];
                  [jsonRes addObject:teamMember];
              }
              
              callback(jsonRes);
          }
      }] resume];
}

- (void)fetchStudio:(void (^)(RIStudio *studio))callback{
    NSString *urlString = [NSString stringWithFormat:@"%@%@", BASE_URL, STUDIO_PATH];
    NSURL *url = [NSURL URLWithString:urlString];
    [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:
      ^(NSData *data, NSURLResponse *response, NSError *error) {
          if (error) {
              NSLog(@"error: %@", error.localizedDescription);
              callback(nil);
              return;
          }
          NSError *jsonError = nil;
          id result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
          
          if ([result isKindOfClass:[NSDictionary class]]) {
              
              RIStudio *studio = [[RIStudio alloc] init];
              [studio loadFromDictionary:result];
              
              callback(studio);
              
          }
      }] resume];
}

- (void)fetchTeamMemberProfile:(void (^)(RITeamMemberProfile *teamMemberProfile))callback withMemberId:(NSString *)memberId{
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@", BASE_URL, TEAM_MEMBER_PROFILE_PATH, memberId];
    NSURL *url = [NSURL URLWithString:urlString];
    [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:
      ^(NSData *data, NSURLResponse *response, NSError *error) {
          if (error) {
              NSLog(@"error: %@", error.localizedDescription);
              callback(nil);
              return;
          }
          NSError *jsonError = nil;
          id result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
          
          if ([result isKindOfClass:[NSDictionary class]]) {
              
              RITeamMemberProfile *profile = [[RITeamMemberProfile alloc] init];
              [profile loadFromDictionary:result];
              
              callback(profile);
              
          }
      }] resume];
}

@end
