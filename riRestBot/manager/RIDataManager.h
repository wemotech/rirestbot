//
//  RIDataManager.h
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 06/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

//This class will centralize all network requests
//We select the block patterns for the communication

#import <Foundation/Foundation.h>

@class RIStudio;
@class RITeamMemberProfile;

@interface RIDataManager : NSObject

extern const NSString *BASE_URL;
extern const NSString *TEAM_MEMBERS_PATH;
extern const NSString *STUDIO_PATH;
extern const NSString *TEAM_MEMBER_PROFILE_PATH;

@property (strong, nonatomic) NSArray *team;

@property (strong, nonatomic) RIStudio *studioData;

+ (id)dataManager;

- (void)fetchAllTeamMembers:(void (^)(NSArray *teamMembers))callback;

- (void)fetchStudio:(void (^)(RIStudio *studio))callback;

- (void)fetchTeamMemberProfile:(void (^)(RITeamMemberProfile *teamMemberProfile))callback withMemberId:(NSString *)memberId;

+ (NSURL *)urlAvatarTeamMember:(NSString *)ridMember;

@end
