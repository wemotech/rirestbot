//
//  main.m
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 04/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RIAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RIAppDelegate class]));
    }
}
