//
//  RITeamViewController.m
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 06/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import "RITeamViewController.h"

#import "RIDataManager.h"
#import "RITeamMemberViewCell.h"
#import "RITeamMember.h"

#import "JMImageCache.h"

#import "GKPopLoadingView.h"

#import "RITeamMemberProfileViewController.h"


@interface RITeamViewController ()

@end

@implementation RITeamViewController

static NSString *CellIdentifier = @"TeamMemberCell";

- (void) viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [self.navigationItem setHidesBackButton:YES];
    [super viewWillAppear:animated];
    
    UIBarButtonItem *studioItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"team_navbar_studio", @"team_navbar_studio") style:UIBarButtonItemStylePlain target:self action:@selector(gotoStudio:)];
    
    NSArray *actionButtonItems = @[studioItem];
    self.navigationItem.rightBarButtonItems = actionButtonItems;
    
    self.title = NSLocalizedString(@"team_title", @"team_title");
    
    [self.navigationController.navigationBar setBarTintColor:UIColorFromRGB(0xf49637)];
    
    [[UINavigationBar appearance] setBarTintColor:[UIColor yellowColor]];

}

- (IBAction)gotoStudio:(id)sender {
    [self performSegueWithIdentifier:@"fromTeamToStudio" sender:self];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dataManager = [RIDataManager dataManager];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [dataManager.team count];;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    RITeamMemberViewCell *cell = (RITeamMemberViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    RITeamMember *teamMember = [dataManager.team objectAtIndex:indexPath.row];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.nameLabel.backgroundColor = [UIColor whiteColor];
    
    if (teamMember.hexColor != nil && teamMember.hexColor.length > 0) {
        unsigned result = 0;
        NSScanner *scanner = [NSScanner scannerWithString:[teamMember.hexColor substringFromIndex:1]];
        [scanner scanHexInt:&result];
        [cell.nameLabel setTextColor:UIColorFromRGB(result)];
    }else{
        cell.nameLabel.textColor = [UIColor blackColor];
    }
    
    if (teamMember.nickname != nil && teamMember.nickname.length > 0) {
        [cell.nameLabel setText:teamMember.nickname];
    }else{
        [cell.nameLabel setText:[NSString stringWithFormat:@"%@ %@", teamMember.lastName, teamMember.firstName]];
    }
    
    [cell.avatarView setImageWithURL:[RIDataManager urlAvatarTeamMember:teamMember.rid] placeholder:[UIImage imageNamed:@"placeholder"]];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    loadingView = [[GKPopLoadingView alloc] init];
    [loadingView show:YES withTitle:nil];
    
    [dataManager fetchTeamMemberProfile:^(RITeamMemberProfile *p){
        profile = p;
        [self performSelectorOnMainThread:@selector(moveToProfileView) withObject:nil waitUntilDone:false];
    }
    withMemberId:((RITeamMember *)[dataManager.team objectAtIndex:indexPath.row]).rid];
    
}

- (void)moveToProfileView{
    [loadingView show:false withTitle:nil];
    [self performSegueWithIdentifier:@"fromTeamToProfile" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"fromTeamToProfile"]) {
        RITeamMemberProfileViewController *vc = (RITeamMemberProfileViewController *)[segue destinationViewController];
        [vc setProfile:profile];
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
