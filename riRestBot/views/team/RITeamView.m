//
//  RITeamView.m
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 07/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import "RITeamView.h"

@implementation RITeamView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
