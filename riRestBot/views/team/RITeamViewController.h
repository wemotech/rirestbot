//
//  RITeamViewController.h
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 06/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RIDataManager;
@class GKPopLoadingView;
@class RITeamMemberProfile;

@interface RITeamViewController : UICollectionViewController <UICollectionViewDataSource, UICollectionViewDelegate>{
    RIDataManager *dataManager;
    GKPopLoadingView *loadingView;
    RITeamMemberProfile *profile;
}

- (IBAction)gotoStudio:(id)sender;
- (void)moveToProfileView;

@end
