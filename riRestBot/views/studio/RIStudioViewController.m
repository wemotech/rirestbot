//
//  RIStudioViewController.m
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 07/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import "RIStudioViewController.h"

#import "RIDataManager.h"
#import "RIStudio.h"

@interface RIStudioViewController ()

@end

@implementation RIStudioViewController

@synthesize studioAddressLabel;

- (void) viewWillAppear:(BOOL)animated {
    self.title = NSLocalizedString(@"studio_title", @"studio_title");
    
    dataManager = [RIDataManager dataManager];
    
    RIStudio *studio = dataManager.studioData;
    
    [studioAddressLabel setText:[NSString stringWithFormat:@"%@ %@\n%@\n%@\n%@\n%@", studio.addressNumber, studio.street, studio.city, studio.county, studio.postcode, studio.country ]];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
