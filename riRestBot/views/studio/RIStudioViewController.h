//
//  RIStudioViewController.h
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 07/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RIDataManager;

@interface RIStudioViewController : UIViewController {
    RIDataManager *dataManager;
}

@property (nonatomic, weak) IBOutlet UILabel *studioAddressLabel;

@end
