//
//  RITeamMemberProfileViewController.h
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 06/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RITeamMemberProfile;

@interface RITeamMemberProfileViewController : UIViewController

@property (strong, nonatomic) RITeamMemberProfile *profile;

@property (nonatomic, weak) IBOutlet UIImageView *avatarView;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *nicknameLabel;
@property (nonatomic, weak) IBOutlet UILabel *roleLabel;

@property (nonatomic, weak) IBOutlet UITextView *descriptionView;

@property (nonatomic, weak) IBOutlet UIButton *twitterButton;
@property (nonatomic, weak) IBOutlet UILabel *favSweetLabel;
@property (nonatomic, weak) IBOutlet UILabel *favSeasonLabel;

/*
id - Unique team member id (required)
firstName - (required)
lastName - (required)
nickname - A nickname the person likes to be called (optional)
hexColor - The persons unique ribot hex colour, prefixed with a # (optional)
role - The persons role at ribot (optional),
description - A short description about the person (optional)
twitter - The persons twitter username (optional)
favSweet - The persons favourite sweet (optional)
favSeason - The persons favourite season of the year. Either spring, summer, autumn or winter (optional)
*/
@end
