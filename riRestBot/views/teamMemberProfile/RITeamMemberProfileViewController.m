//
//  RITeamMemberProfileViewController.m
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 06/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import "RITeamMemberProfileViewController.h"

#import "RITeamMemberProfile.h"
#import "RIDataManager.h"

#import "JMImageCache.h"

@interface RITeamMemberProfileViewController ()

@end

@implementation RITeamMemberProfileViewController

@synthesize profile;

@synthesize avatarView;
@synthesize nameLabel;
@synthesize nicknameLabel;
@synthesize roleLabel;
@synthesize descriptionView;
@synthesize twitterButton;
@synthesize favSweetLabel;
@synthesize favSeasonLabel;

//- (void)viewDidAppear:(BOOL)animated {
- (void)viewWillAppear:(BOOL)animated{

    if (profile.nickname != nil && profile.nickname.length > 0) {
        self.title = profile.nickname;
        [nicknameLabel setText:profile.nickname];
    }else{
        [nicknameLabel setText:@""];
        self.title = [NSString stringWithFormat:@"%@ %@", profile.lastName, profile.firstName];
    }
    
    [nameLabel setText:[NSString stringWithFormat:@"%@ %@", profile.lastName, profile.firstName]];
    
    [avatarView setImageWithURL:[RIDataManager urlAvatarTeamMember:profile.rid] placeholder:[UIImage imageNamed:@"placeholder"]];
    
    if (profile.role != nil && profile.role.length > 0) {
        [roleLabel setText:profile.role];
    }else{
        [roleLabel setText:@""];
    }
    
    if (profile.favSweet != nil && profile.favSweet.length > 0) {
        [favSweetLabel setText:[NSString stringWithFormat:@"%@\n%@", NSLocalizedString(@"team_member_profile_fav_sweet", @"team_member_profile_fav_sweet"), profile.favSweet]];
    }else{
        [favSweetLabel setText:@""];
    }
    
    if (profile.favSeason != nil && profile.favSeason.length > 0) {
        [favSeasonLabel setText:[NSString stringWithFormat:@"%@\n%@", NSLocalizedString(@"team_member_profile_fav_season", @"team_member_profile_fav_season"), profile.favSeason]];
    }else{
        [favSeasonLabel setText:@""];
    }
    
    if (profile.favSeason != nil && profile.favSeason.length > 0) {
        [twitterButton.titleLabel setText:profile.twitter];
        [twitterButton setTitle:profile.twitter forState:UIControlStateNormal];
    }else{
        [twitterButton setHidden:true];
    }
    
    if (profile.description != nil && profile.description.length > 0) {
        [descriptionView setText:profile.description];
    }else{
        [descriptionView setHidden:true];
    }
    
    
    
    
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
