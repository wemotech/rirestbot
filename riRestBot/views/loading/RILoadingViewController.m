//
//  RILoadingViewController.m
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 06/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import "RILoadingViewController.h"

#import "RIDataManager.h"

@implementation RILoadingViewController

@synthesize bgPicture;
@synthesize loadingIndicator;
@synthesize loadingLabel;

- (void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    
    [bgPicture setFrame:self.view.frame];
    //We should have used LaunchImage but more complicated in fact.
    [bgPicture setImage:[UIImage imageNamed:@"LoadingPlaceHolder"]];
    
    loadingLabel.textColor = UIColorFromRGB(0x00a3d9);
    loadingLabel.backgroundColor = [UIColor clearColor];
    loadingLabel.shadowColor = [UIColor blackColor];
    loadingLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    
    [loadingLabel setText:NSLocalizedString(@"loading_in_progress", @"loading_in_progress")];
    
    [loadingIndicator setHidden:true];
    [loadingLabel setHidden:true];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [loadingIndicator setHidden:false];
    [loadingLabel setHidden:false];
    
    RIDataManager *dataManager = [RIDataManager dataManager];
    [dataManager fetchAllTeamMembers:^(NSArray *teamMembers){
        
        dataManager.team = teamMembers;
        /*
         for (RITeamMember* member in teamMembers) {
         
         //[member logIt];
         
         [dataManager fetchTeamMemberProfile:^(RITeamMemberProfile *profile){
         //[profile logIt];
         }
         withMemberId:member.rid];
         
         }
         */
        
        [dataManager fetchStudio:^(RIStudio *s){
            
            dataManager.studioData = s;
            
            [self performSelectorOnMainThread:@selector(moveForward) withObject:nil waitUntilDone:false];
            
        }];
        
        
    }];
    
}

- (void)moveForward {
    
    [loadingIndicator setHidden:true];
    [loadingLabel setHidden:true];
    
    [self performSegueWithIdentifier:@"fromLoadingToTeam" sender:self];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
