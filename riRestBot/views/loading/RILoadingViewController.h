//
//  RILoadingViewController.h
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 06/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RILoadingViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIImageView *bgPicture;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (nonatomic, weak) IBOutlet UILabel *loadingLabel;

- (void)moveForward;

@end
