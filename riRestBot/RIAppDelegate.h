//
//  RIAppDelegate.h
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 04/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RIAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
