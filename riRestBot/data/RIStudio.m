//
//  RIStudio.m
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 06/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import "RIStudio.h"

@implementation RIStudio

//@synthesize would be replaced by @dynamic for CoreData
@synthesize addressNumber;
@synthesize street;
@synthesize city;
@synthesize county;
@synthesize postcode;
@synthesize country;
@synthesize photos;

- (void)loadFromDictionary:(NSDictionary *)dictionary
{
    self.addressNumber = dictionary[@"addressNumber"];
    self.street = dictionary[@"street"];
    self.city = dictionary[@"city"];
    self.county = dictionary[@"county"];
    self.postcode = dictionary[@"postcode"];
    self.country = dictionary[@"country"];
    self.photos = dictionary[@"photos"];
}

- (void)logIt{
    NSLog(@"RIStudio");
    NSLog(@"    self.addressNumber %@", self.addressNumber);
    NSLog(@"    self.street %@", self.street);
    NSLog(@"    self.city %@", self.city);
    NSLog(@"    self.county %@", self.county);
    NSLog(@"    self.postcode %@", self.postcode);
    NSLog(@"    self.country %@", self.country);
    NSLog(@"    self.photos %@", self.photos);
}

@end
