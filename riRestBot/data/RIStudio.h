//
//  RIStudio.h
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 06/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import "RIBaseObject.h"

@interface RIStudio : RIBaseObject

//The number on the street (required)
@property (nonatomic, retain) NSNumber * addressNumber;

//The street name (required)
@property (nonatomic, retain) NSString * street;

//(required)
@property (nonatomic, retain) NSString * city;

//(required)
@property (nonatomic, retain) NSString * county;

//(required)
@property (nonatomic, retain) NSString * postcode;

//(required)
@property (nonatomic, retain) NSString * country;

//An array of URLs to photos of the studio (optional)
@property (nonatomic, retain) NSArray * photos;

- (void)loadFromDictionary:(NSDictionary *)dictionary;

//Used for CoreData
//+ (RIStudio *)findOrCreateStudioWithIdentifier:(NSString *)identifier inContext:(NSManagedObjectContext *)context;

- (void)logIt;

@end
