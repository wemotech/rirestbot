//
//  RITeamMemberProfile.h
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 06/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import "RITeamMember.h"

@interface RITeamMemberProfile : RITeamMember

//A short description about the person (optional)
@property (nonatomic, retain) NSString * description;

//The persons twitter username (optional)
@property (nonatomic, retain) NSString * twitter;

//The persons favourite sweet (optional)
@property (nonatomic, retain) NSString * favSweet;

//The persons favourite season of the year. Either spring, summer, autumn or winter (optional)
@property (nonatomic, retain) NSString * favSeason;

- (void)loadFromDictionary:(NSDictionary *)dictionary;

- (void)logIt;

@end
