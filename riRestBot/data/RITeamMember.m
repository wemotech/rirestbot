//
//  RITeamMember.m
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 06/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import "RITeamMember.h"

@implementation RITeamMember

@synthesize rid;
@synthesize firstName;
@synthesize lastName;
@synthesize nickname;
@synthesize hexColor;
@synthesize role;

- (void)loadFromDictionary:(NSDictionary *)dictionary{
    [self setValue:dictionary[@"id"] forKey:@"rid"];
    //self.rid = dictionary[@"id"];
    [self setValue:dictionary[@"firstName"] forKey:@"firstName"];
    //self.firstName = dictionary[@"firstName"];
    [self setValue:dictionary[@"lastName"] forKey:@"lastName"];
    //self.lastName = dictionary[@"lastName"];
    [self setValue:dictionary[@"nickname"] forKey:@"nickname"];
    //self.nickname = dictionary[@"nickname"];
    [self setValue:dictionary[@"hexColor"] forKey:@"hexColor"];
    //self.hexColor = dictionary[@"hexColor"];
    [self setValue:dictionary[@"role"] forKey:@"role"];
    //self.role = dictionary[@"role"];
}

- (void)logIt{
    NSLog(@"RITeamMember");
    NSLog(@"    self.rid %@", self.rid);
    NSLog(@"    self.firstName %@", self.firstName);
    NSLog(@"    self.lastName %@", self.lastName);
    NSLog(@"    self.nickname %@", self.nickname);
    NSLog(@"    self.hexColor %@", self.hexColor);
    NSLog(@"    self.role %@", self.role);
}

@end
