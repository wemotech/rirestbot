//
//  RITeamMemberProfile.m
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 06/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import "RITeamMemberProfile.h"

@implementation RITeamMemberProfile

@synthesize description;
@synthesize twitter;
@synthesize favSweet;
@synthesize favSeason;

- (void)loadFromDictionary:(NSDictionary *)dictionary{
    [super loadFromDictionary:dictionary];
    [self setValue:dictionary[@"description"] forKey:@"description"];
    //self.description = dictionary[@"description"];
    [self setValue:dictionary[@"twitter"] forKey:@"twitter"];
    //self.twitter = dictionary[@"twitter"];
    [self setValue:dictionary[@"favSweet"] forKey:@"favSweet"];
    //self.favSweet = dictionary[@"favSweet"];
    [self setValue:dictionary[@"favSeason"] forKey:@"favSeason"];
    //self.favSeason = dictionary[@"favSeason"];
}

- (void)logIt{
    NSLog(@"RITeamMemberProfile");
    [super logIt];
    NSLog(@"    self.description %@", self.description);
    NSLog(@"    self.twitter %@", self.twitter);
    NSLog(@"    self.favSweet %@", self.favSweet);
    NSLog(@"    self.favSeason %@", self.favSeason);
}

@end
