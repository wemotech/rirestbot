//
//  RITeamMember.h
//  riRestBot
//
//  Created by Frédéric ABRIOUX on 06/07/2014.
//  Copyright (c) 2014 wemoTech LTD. All rights reserved.
//

#import "RIBaseObject.h"

@interface RITeamMember : RIBaseObject

//Unique team member id (required)
@property (nonatomic, retain) NSString * rid;

//(required)
@property (nonatomic, retain) NSString * firstName;

//(required)
@property (nonatomic, retain) NSString * lastName;

//A nickname the person likes to be called (optional)
@property (nonatomic, retain) NSString * nickname;

//The persons unique ribot hex colour, prefixed with a # (optional)
@property (nonatomic, retain) NSString * hexColor;

//The persons role at ribot (optional)
@property (nonatomic, retain) NSString * role;

- (void)loadFromDictionary:(NSDictionary *)dictionary;

- (void)logIt;

@end
